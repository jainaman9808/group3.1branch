import './App.css';
import About from './Components/About-Us';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
    <About />
    </BrowserRouter>
  );
}

export default App;
