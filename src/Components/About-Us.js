import React, { Component } from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader} from 'reactstrap';
import { Link } from 'react-router-dom';
import axios from 'axios';


const api = axios.create({
    baseURL: 'http://localhost:3001/about/'
})


class About extends Component {


    state = {
        about: []
    }

    constructor() {
        super();
        api.get('/').then( res => {
            console.log(res.data)
            this.setState({ about: res.data })
        })
    }

    render() {
        return(
            <div className="container">
            <div className="row back_colour">
                <Breadcrumb>
                    <BreadcrumbItem active>About Us</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12 ml-3 text-white">
                    <h3>About Us</h3>
                    <hr />
                </div>                
            </div>
            <div className="row row-content">
                <div className="col-12 col-md-6 ml-3">
                    <h2>Our History</h2>
                    { this.state.about.map(about_us => <p>{about_us.description}</p>)}
                </div>
                <div className="col-12 col-md-5 mt-3">
                    <Card>
                        <CardHeader body inverse style={{ backgroundColor: 'grey', borderColor: 'grey' }} className="text-white">Facts At a Glance</CardHeader>
                        <CardBody className="back_clr">
                            <dl className="row p-1">
                                <dt className="col-6">Website</dt>
                                <dd className="col-6"><Link>http://www.fundingx.in</Link></dd>
                                <dt className="col-6">Industry</dt>
                                <dd className="col-6">Fundraising</dd>
                                <dt className="col-6">Company size</dt>
                                <dd className="col-6">2-10 employees</dd>
                                <dt className="col-6">Type</dt>
                                <dd className="col-6">Privately Held</dd>
                            </dl>
                        </CardBody>
                    </Card>
                </div>
                <div className="col-11 mt-4 ml-3">
                    <Card>
                        <CardBody className="bg-faded back_clr">
                            <blockquote className="blockquote">
                                <p className="mb-0">"Let our advance worrying become advance thinking and planning."</p>
                                <footer className="blockquote-footer"> ~ Winston Churchill..
                                </footer>
                            </blockquote>
                        </CardBody>
                    </Card>
                </div>
                <div className="col-12 mt-4 d-flex justify-content-center">
                { this.state.about.map(about_us => 
                <Link className="m-5">{about_us.links}</Link>)}
                </div>
            </div>
            </div>
        )
    }
}

export default About;    